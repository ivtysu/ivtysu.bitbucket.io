function showFromGD(button) {
    console.log(button);
    fileId = button.parent().parent().attr('id');
    window.open(`https://drive.google.com/open?id=${fileId}`, '_blank');
}

function downloadFromGD(button) {
    fileId = button.parent().parent().attr('id');
    window.open(`https://drive.google.com/uc?export=download&id=${fileId}`, '_blank');
}

function generateCard(idNamesMap) {
    Object.keys(idNamesMap).forEach(name => {
        
    });
}

$(document).ready(() => {
    buttons =
        `<div class="secondary-content"> <a class='waves-effect waves-light' onclick='javascript:showFromGD($(this))'><i class='material-icons pink-text accent-2'>open_in_browser</i></a> 
    <a class='waves-effect waves-light' onclick='javascript:downloadFromGD($(this))'><i class='material-icons pink-text accent-2'>file_download</i></a> </div>`
    $('li').filter('.collection-item').append(buttons);
});